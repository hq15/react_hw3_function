import React from "react";
import PropTypes from "prop-types";
import "./CardProduct.scss";
import Button from "../button/Button.jsx";
import "../button/Button.scss";
import WishlistIcon from "../wishlistIcon/WishlistIcon.jsx";

const CardProduct = (props) => {
  const {
    card,
    onClick,
    favorites,
    closeButtonAddCart,
    className,
    isItemInCart,
    
  } = props;

  return (
    <div key={card.id} className={className} id={card.id}>
      <h3 className="product-name">{card.ProductName}</h3>
      <div className="product-card-container">
        <div>
          <div className="img-wrapper">
            <img
              className="product-img"
              src={card.link}
              alt={card.ProductName}
            />
          </div>
        </div>
        <div>
          <p className="product-article">Model: {card.type}</p>
          <p>Price: {card.price} USD</p>
          <p>Color: {card.color}</p>
        </div>
      </div>
      <div id={card.id} className="product-activity">
        {closeButtonAddCart && (
          <Button
            id={card.id}
            className={` btn ${
              isItemInCart ? "btn__added-to-card" : "btn__add-to-card"
            }`}
            dataModal={"modalID2"}
            text={`${isItemInCart ? "Added to cart" : "Add to cart"}`}
            onClick={isItemInCart ? null : onClick}
          />
        )}

        {isItemInCart && (
          <button
            id={card.id}
            className="btn__close"
            data-modal-id={"modalID3"}
            onClick={onClick}
          >
            X
          </button>
        )}
        <WishlistIcon
          id={card.id}
          classNameIcon={"favor-icon"}
          isFavorites={card.isFavorites}
          onClick={() => favorites(card.id)}
        />
      </div>
    </div>
  );
};

CardProduct.propTypes = {
  card: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  classNameIcon: PropTypes.string,
  favorites: PropTypes.func,
  isFavorites: PropTypes.bool,
  className: PropTypes.string,
};
CardProduct.defaultProps = {
  classNameIcon: "favor-icon",
  className: "product-card",
  isFavorites: false,
};

export default CardProduct;
