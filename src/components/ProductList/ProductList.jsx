import React from "react";
import PropTypes from "prop-types";
import CardProduct from "../CardProduct/CardProduct";
import "./ProductList.scss";

function ProductList(props) {
  const cartProds = JSON.parse(localStorage.getItem("cart"));

  const {
    cards,
    onClick,
    favoritesListId,
    onFavoritesChange,
    classNameIcon,
    removeToCart,
    closeButtonAddCart,
    className,
  } = props;

  const cardsWithIsFavorites = cards.map((element) => {
    favoritesListId.includes(element.id)
      ? (element.isFavorites = true)
      : (element.isFavorites = false);
    return element;
  });
  const productsList = cardsWithIsFavorites;

  const addToFavorites = (id) => {
    let newProducts = favoritesListId.map((id) => id);
    if (newProducts.includes(id)) {
      newProducts = newProducts.filter((item) => item !== id);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    } else {
      newProducts.push(id);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    }
    onFavoritesChange(newProducts);
  };

  const cardsList = productsList.map((e) => (
    <CardProduct
      key={e.id}
      card={e}
      className={className}
      onClick={onClick}
      favorites={addToFavorites}
      classNameIcon={classNameIcon}
      removeToCart={removeToCart}
      closeButtonAddCart={closeButtonAddCart}
      isItemInCart={cartProds.includes(e.id)}
    />
  ));
  return <div className="products-wrapper">{cardsList}</div>;
}

ProductList.propTypes = {
  cards: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    })
  ).isRequired,
  onClick: PropTypes.func,
  closeButtonAddCart: PropTypes.bool,
};

ProductList.defaultProps = {
  onClick: null,
  closeButtonAddCart: true,
};

export default ProductList;
