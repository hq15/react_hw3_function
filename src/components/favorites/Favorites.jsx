import React from "react";
import ProductList from "../ProductList/ProductList";

function Favorites(props) {
  const {
    cards,
    favoritesListId,
    onFavoritesChange,
    onClick,
    classNameIcon,
    filter,
  } = props;

  const favoritesListProd = filter(cards, favoritesListId);

  return (
    <>
      <h2>YOUR FAVORITES</h2>
      <ProductList
        cards={favoritesListProd}
        favoritesListId={favoritesListId}
        onFavoritesChange={onFavoritesChange}
        onClick={onClick}
        classNameIcon={classNameIcon}
        closeButtonAddCart={true}
      />
    </>
  );
}

export default Favorites;
