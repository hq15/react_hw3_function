import React from "react";
import PropTypes from "prop-types";
import "./Button.scss";

const Button = (props) => {
  const { id, className, dataModal, onClick, text } = props;
  return (
    <button
      id={id}
      className={className}
      data-modal-id={dataModal}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  dataModal: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

Button.defaultProps = {
  id: null,
  className: "modal__btn",
  dataModal: "modalID2",
  text: "Add to cart",
};

export default Button;
