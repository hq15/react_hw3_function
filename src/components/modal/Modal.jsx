import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./Modal.scss";

function Modal(props) {
  const {
    className,
    header,
    closeButton,
    onClick,
    description,
    actions,
    show,
    hidden,
  } = props;

  const [modalState, setModalState] = useState("none");

  useEffect(() => {
    show();
    setModalState("open");
    return function cleanup() {
      hidden();
      setModalState("none");
    };
  }, []);

  return (
    <>
      <div className={className}>
        <div className="modal__header">
          <h3>{header}</h3>
          {closeButton && (
            <button className="btn__close" onClick={onClick}>
              X
            </button>
          )}
        </div>
        <p className="modal__text">{description}</p>
        {actions}
      </div>
    </>
  );
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  actions: PropTypes.object.isRequired,
  closeButton: PropTypes.bool,
};

Modal.defaultProps = {
  closeButton: false,
};

export default Modal;
