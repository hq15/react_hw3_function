import React from "react";
import ProductList from "../ProductList/ProductList";

const Cart = (props) => {
  const {
    cards,
    favoritesListId,
    onFavoritesChange,
    onClick,
    classNameIcon,
    cartProdsId,
    filter,
  } = props;

  const cartProducts = filter(cards, cartProdsId);

  return (
    <>
      <h2>YOUR CART</h2>
      <ProductList
        cards={cartProducts}
        favoritesListId={favoritesListId}
        onFavoritesChange={onFavoritesChange}
        onClick={onClick}
        classNameIcon={classNameIcon}
        closeButtonAddCart={false}
      />
    </>
  );
};

export default Cart;
