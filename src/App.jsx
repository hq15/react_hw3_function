import React, { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import axios from "axios";
import Header from "./components/header/Header";
import ProductList from "./components/ProductList/ProductList";
import Cart from "./components/Cart/Cart";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import modalTemplates from "./components/modal/modalTemplates.js";
import Favorites from "./components/favorites/Favorites";

let modalDeclaration = {};

const [modalWindowDeclarations] = Object.values(modalTemplates);

const App = () => {
  const [cards, setCards] = useState([]);
  const [displayState, setDisplayState] = useState("none");
  const [classWrapper, setClassWrapper] = useState("");
  const [targetProd, setTargetProd] = useState();
  const [cartProdsId, setCartProdsId] = useState([]);
  const [favoritesListId, setFavoritesListId] = useState([]);

  useEffect(() => {
    axios("/airplane.json").then((res) => {
      setCards(res.data);
    });
    if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", "[]");
  }, []);

  useEffect(() => {
    const cartProdsIdUpdate = JSON.parse(localStorage.getItem("cart"));
    setCartProdsId(cartProdsIdUpdate);
  }, []);

  useEffect(() => {
    const favoritesProd = JSON.parse(localStorage.getItem("favorites"));
    setFavoritesListId(favoritesProd);
  }, []);

  // background of closing content when modal is running
  const didMount = () => {
    setClassWrapper("wrapper");
  };

  const didUnmounted = () => {
    setClassWrapper("");
  };
  // -------------------------------

  //selection of the required type of modal window
  const openModal = (e) => {
    const modalID = e.target.dataset.modalId;
    modalDeclaration = modalWindowDeclarations.find(
      (item) => item.id === modalID
    );
    setTargetProd(e.target.id);
    setDisplayState("open");
  };

  const closeModal = () => {
    setDisplayState("none");
  };
  // ----------------------------------

  // activity of a button of a modal window depending on its type
  const onClickModalBtn = (e) => {
    let newProds = cartProdsId.map((element) => element);
    setTargetProd(e.target.id);
    if (e.target.dataset.modalId === "modalID2") {
      newProds.push(targetProd);
      localStorage.setItem("cart", JSON.stringify(newProds));
    }
    if (e.target.dataset.modalId === "modalID3") {
      newProds = cartProdsId.map((element) => element);
      if (newProds.includes(targetProd)) {
        newProds = newProds.filter((item) => item !== targetProd);
        localStorage.setItem("cart", JSON.stringify(newProds));
      }
    }
    setDisplayState("none");
    setCartProdsId(newProds);
  };
  //-----------------------------------------

  //array typing for components cart and favorites
  const filterBy = (a, b) => {
    let typedArr = a.filter(function (a) {
      return a.id === b.find((item) => item === a.id);
    });
    return typedArr;
  };
  //--------------------------------------------------
  const {
    className,
    id,
    header,
    closeButton,
    description,
    classNameButton,
    textButtonLeft,
    textButtonRight,
  } = modalDeclaration;

  const classNameIcon = "favor-icon";

  return (
    <>
      <div className={classWrapper} onClick={closeModal}></div>
      <Header />
      <div className="content">
        <Routes>
          <Route
            path="/"
            element={
              <ProductList
                cards={cards}
                favoritesListId={favoritesListId}
                onFavoritesChange={setFavoritesListId}
                onClick={openModal}
                classNameIcon={classNameIcon}
                closeButtonAddCart={true}
              />
            }
          ></Route>
          <Route
            path="cart"
            element={
              <Cart
                cards={cards}
                favoritesListId={favoritesListId}
                onFavoritesChange={setFavoritesListId}
                onClick={openModal}
                classNameIcon={classNameIcon}
                cartProdsId={cartProdsId}
                filter={filterBy}
              />
            }
          ></Route>
          <Route
            path="favorites"
            element={
              <Favorites
                cards={cards}
                favoritesListId={favoritesListId}
                onFavoritesChange={setFavoritesListId}
                onClick={openModal}
                classNameIcon={classNameIcon}
                cartProdsId={cartProdsId}
                filter={filterBy}
              />
            }
          ></Route>
        </Routes>
      </div>

      {displayState === "open" && (
        <Modal
          className={className}
          id={id}
          onClick={closeModal}
          show={didMount}
          hidden={didUnmounted}
          header={header}
          closeButton={closeButton}
          description={description}
          actions={
            <>
              <Button
                id={id}
                className={classNameButton}
                dataModal={id}
                text={textButtonLeft}
                onClick={onClickModalBtn}
              />
              <Button
                className={classNameButton}
                text={textButtonRight}
                onClick={closeModal}
              />
            </>
          }
        />
      )}
    </>
  );
};

export default App;
